package ua.rv.huts.oleh;

import java.util.ArrayList;
import java.lang.Math;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {

        ArrayList<Integer> randomNumbers = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            int num = (int) (Math.random() * 14);
            randomNumbers.add(num);
        }
        System.out.println();
        System.out.println("Random number:");
        for(int i = 0; i < randomNumbers.size(); i++){
            System.out.print(randomNumbers.get(i));
            System.out.print(" ");
        }
        System.out.println();
        System.out.println("\nMap x*x, and reduce:");
        Optional<Integer> reduced =
                randomNumbers
                        .stream()
                        .map(x->x*x)
                        .reduce((i1, i2) -> i1 + i2);
        reduced.ifPresent(System.out::println);



        System.out.println();
        System.out.println("Filter count even:");
        long filterRandomNumber = randomNumbers
                .stream()
                .filter(i->i % 2 == 0).count();
        System.out.println(filterRandomNumber);



        String [] arrayOfNames = {"Adam", "Jack", "Robert", "Katy", "Lily", "Julia", "Joy"};

        List<String> groupOfNames = new ArrayList<>();
        for(String person: arrayOfNames){
            groupOfNames.add(person);
        }

        System.out.println();
        System.out.println("Names:");
        groupOfNames
                .stream()
                .filter((s) -> s.startsWith("J"))
                .forEach(System.out::println);

    }
}

